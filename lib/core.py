#!../.env/bin/python3
import json
import logging
import websockets.exceptions

from lib import protocol
from lib.client import Client, ClientManager

### Logging
logger = logging.getLogger(__name__)
d = logger.debug

client_manager = ClientManager()

async def clientHandler(socket, path):
    d('Client connected from {} ({})'.format(socket.remote_address,
                                             socket.local_address))
    client = client_manager.register(socket)

    d('Awaiting handshake')
    await client.handshake()
    d('Handshake complete')

    d('Searching for partner')
    candidate = await client_manager.findPartner(client)
    d('Search for partner complete')

    if candidate:
        d('Found partner')
        client.assignPartner(candidate)
        client_manager.removeFromQueue(candidate)

        await client.broadcast({'event': protocol.CONNECTED})
    else:
        d('No partner found')
        client_manager.addToQueue(client)
        await client.send({'event': protocol.WAITING_FOR_PARTNER})

    while True:
        try:
            data = json.loads(await socket.recv())
        except websockets.exceptions.ConnectionClosed:
            d('Connection closed')
            if client.hasPartner():
                await client.partner.send({'event': protocol.PARTNER_DISCONNECT})

            break

        if client.hasPartner():
            if 'local' in data:
                del data['local']
            await client.partner.send(data)
        else:
            await client.send({
                'event': protocol.ERROR,
                'result': protocol.NO_PARTNER,
            })

    # Cleaning up
    client_manager.unregister(client)
    client_manager.removeFromQueue(client)
