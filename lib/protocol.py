### 1xx Client commands
HANDSHAKE           = 101

### 2xx Events 
MESSAGE             = 201 # Message between clients 
CONNECTED           = 202 # Found partner
WAITING_FOR_PARTNER = 203 # Could not find partner, please wait
PARTNER_DISCONNECT  = 204 # Partner disconnected

### 3xx Message types
ERROR               = 301

### 4xx Client error
NO_PARTNER          = 401
