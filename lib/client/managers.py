import logging

from lib.client import Client

logger = logging.getLogger(__name__)

d = logger.debug

class ClientManager():
    def __init__(self):
        self.__clients = list() 
        self.__rooms = dict()

    def register(self, socket):
        client = Client(socket)

        self.__clients.append(client)

        return client
    def unregister(self, client):
        self.__clients.remove(client)
        if client in self.__rooms[client.subject]:
            self.__rooms[client.subject].remove(client)

    async def findPartner(self, client):
        while True:
            try:
                candidate = self.__rooms[client.subject].pop(0)

                if candidate is not client:
                    if await candidate.isActive():
                        return candidate 
                    else:
                        self.unregister(candidate)
                        self.removeFromQueue(candidate)
            except KeyError:
                d('Room {} does not exist')

                break
            except IndexError:
                d('')

        return None

    def addToQueue(self, client):
        if client.subject not in self.__rooms:
            self.__rooms[client.subject] = list()

        self.__rooms[client.subject].append(client)
    def removeFromQueue(self, client):
        if client in self.__rooms[client.subject]:
            self.__rooms[client.subject].remove(client)

        if len(self.__rooms[client.subject]) == 0:
            del self.__rooms[client.subject]
