import json
import logging

### Logging
logger = logging.getLogger(__name__)
d = logger.debug

class Client():
    def __init__(self, socket):
        self.__socket = socket
        self.__partner = None
    
    @property
    def __get_partner(self):
        return self.__partner

    def assign_partner(self, target):
        '''
            Secure symmetrical relationship between partners
        '''
        self.__partner = target
        target.__partner = self

    def hasPartner(self):
        '''
            Returns true if the client has a partner
        '''
        return self.partner is not None 

    async def isActive(self):
        response = await self.__socket.ping()

        d('Ping response is: {}'.format(response))

        return response is not None

    async def handshake(self):
        '''
            Performs the required actions to setup a chat
        '''
        package = json.loads(await self.__socket.recv())

        self.subject = package['subject']

    async def send(self, raw_package):
        '''
            Sends a package to the client
        '''
        package = json.dumps(raw_package)

        await self.__socket.send(package)

    async def broadcast(self, raw_package):
        '''
            Broadcasts a message to both the client and it's partner
        '''
        package = json.dumps(raw_package)

        await self.__socket.send(package)
        await self.partner.__socket.send(package)
