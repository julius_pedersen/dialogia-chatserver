LOG_DIR=log
LOG_FILE=$(LOG_DIR)/chatserver.txt
UNIT_FILE=/usr/lib/systemd/system/dialogia-chatserver.service

all:
	virtualenv .env && source .env/bin/activate && pip install -r requirements.txt && \
	mkdir $(LOG_DIR)

install:
	cp resources/dialogia-chatserver.service $(UNIT_FILE)

uninstall:
	rm $(UNIT_FILE) && \

clean:
	rm -r .env && rm -r $(LOG_DIR)
